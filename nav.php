<?php

require("bdd/bddconfig.php");

try {
    $objBdd = new PDO("mysql:host=$bddserver;dbname=$bddname;charset=utf8", $bddlogin, $bddpass);
    $objBdd->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    $recup = $objBdd->query("SELECT * FROM `theme` ");
} catch (Exception $prmE) {
    die("Erreur : " . $prmE->getMessage());
}

?>

<nav>
    <ul>

        <a href="index.php?page=accueil">Accueil</a>

        <?php
        while ($lien = $recup->fetch()) {
        ?>

            <li class="lien">
                <a href="index.php?page=theme&value=<?php echo $lien["nom"]; ?>&idTheme=<?php echo $lien["idTheme"]; ?>&idTheme=<?php echo $lien["idTheme"]; ?>"><?php echo $lien["nom"] ?></a>
            </li>

        <?php
        };
        $recup->closeCursor();
        ?>


        <?php 
    
        
        if(isset($_SESSION['logged_in']['fonction'])){

            if($_SESSION['logged_in']['fonction'] == "admin" ){
    
            ?>
            <br><br>
            <a href="index.php?page=formNewTheme">Ajouter un thème</a> <br>
            <a href="index.php?page=editNameTheme">Modifier un thème</a><br>
            <a href="index.php?page=selectSuppTheme">Supprimer un thème</a>
             
            <br><br>

            <a href="index.php?page=form_user_admin">Ajouter un compte</a><br>
            <a href="index.php?page=formEditSuppAccount">Supprimer / Modifier un compte</a><br>

        <?php 
            }
        }
        ?>

    </ul>
</nav>
