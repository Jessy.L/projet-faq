<?php 


require("bdd/bddconfig.php");

try {
    $objBdd = new PDO("mysql:host=$bddserver;dbname=$bddname;charset=utf8", $bddlogin, $bddpass);
    $objBdd->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    $recup = $objBdd->query("SELECT * FROM `user`");
    $recupSupp = $objBdd->query("SELECT * FROM `user`");
    
} catch (Exception $prmE) {
    die("Erreur : " . $prmE->getMessage());
}


?>


<main>

    <form method="POST" action="admin/editAccount.php">

        <select name="user">
            
                <?php
                while ($user = $recup->fetch()) {
                ?>

                    <option value="<?php echo $user["idUser"] ?>"><?php echo $user["nom"]?></option>

                <?php
                };
                $recup->closeCursor();
                ?>
        </select>
        
        <label for="nom">Nom</label>
        <input type="text" name="nom" required>

        <label for="login">Login</label>
        <input type="text" name="login" required>

        <label for="password">MOT DE PASSE</label>
        <input type="text" name="password" required>
        
        <label for="fonction">Fonction</label>
        <select name="fonction" required>
            <option value="admin">Admin</option>
            <option value="tech">Tech</option>
        </select>

        <input type="submit" value="MODIFIER">

    </form>


    <form method="POST" action="admin/deleteUser.php">

        <select name="user">
                
                <?php
                while ($user = $recupSupp->fetch()) {
                ?>

                    <option value="<?php echo $user["idUser"] ?>"><?php echo $user["nom"]?></option>

                <?php
                };
                $recupSupp->closeCursor();
                ?>
        </select>

        <input type="submit" value="SUPPRIMER">

    </form>

</main>