<?php



$_SESSION['idArticleAddDoc'] = $_GET["idArticle"];

require("bdd/bddconfig.php");

try {
    $objBdd = new PDO("mysql:host=$bddserver;dbname=$bddname;charset=utf8", $bddlogin, $bddpass);
    $objBdd->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    $recup = $objBdd->query("SELECT * FROM `article` WHERE idArticle =" . $_GET["idArticle"]);
} catch (Exception $prmE) {
    die("Erreur : " . $prmE->getMessage());
}


?>



<main>

    <form method="POST" action="gestion_article/editArticle.php">

        <?php
        while ($data = $recup->fetch()) {
        ?>

            <input type="text" name="titre" value="<?php echo $data["titre"] ?>">
            <textarea name="commentaire" cols="30" rows="10"><?php echo $data["texte"] ?></textarea>
            <input type="hidden" name="idArticle" value="<?php echo $_GET["idArticle"] ?>">

            <select name="acces" value="<?php echo $data["acces"] ?>">
                <option value="public">Public</option>
                <option value="private">Private</option>
            </select>
            <input type="submit">

        <?php
        }
        $recup->closeCursor()
        ?>
    </form>

    <a href="index.php?page=formAddDocument">Ajouter ou Supprimer des documents</a>

    <form method="POST" id="formDelete">
        <label for="check">
            <input type="checkbox" name="check" id="check">
            <input type="hidden" name="idArticle" value="<?php echo $_GET["idArticle"] ?>">
            Voulez vous vraiment supprimer l'article ?
        </label>
        <input type="submit" value="SUPPRIMER">
    </form>



    <script src="js/delete.js"></script>
</main>