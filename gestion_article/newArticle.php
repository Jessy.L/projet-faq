<?php
session_start();

$titre = htmlspecialchars($_POST["titre"]);
$commentaire = htmlspecialchars($_POST["commentaire"]);
$acces = htmlspecialchars($_POST["acces"]);

$idUser = $_SESSION['logged_in']['id'];
$theme = htmlspecialchars($_POST["theme"]);


require('../bdd/bddconfig.php');

try {

    $objBdd = new PDO("mysql:host=$bddserver;dbname=$bddname;charset=utf8", $bddlogin, $bddpass);
    $objBdd->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    $PDOinsert = $objBdd->prepare("INSERT INTO `article` (titre, texte, acces, theme_idTheme, user_idUser) VALUES (:titre, :texte, :acces, :theme, :idUser )");
        $PDOinsert->bindParam(':titre', $titre, PDO::PARAM_STR);
        $PDOinsert->bindParam(':texte', $commentaire, PDO::PARAM_STR);
        $PDOinsert->bindParam(':acces', $acces, PDO::PARAM_STR);
        $PDOinsert->bindParam(':theme', $theme, PDO::PARAM_STR);
        $PDOinsert->bindParam(':idUser', $idUser, PDO::PARAM_STR);

        $PDOinsert->execute();


        header("Location: ../index.php");

} catch (Exception $prmE) {
    die('Erreur : ' . $prmE->getMessage());
}


// $serveur = $_SERVER['HTTP_HOST'];
// $chemin = rtrim(dirname(htmlspecialchars($_SERVER['PHP_SELF'])), '/\\');
// $page = 'index.php';
// header("Location: http://$serveur$chemin/$page");


header("Location: ../index.php");