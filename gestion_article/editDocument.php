<?php 

session_start();

$nom = htmlspecialchars( $_POST["nom"]);
$url = htmlspecialchars( $_POST["url"]);
$type = htmlspecialchars( $_POST["type"]);
$idDoc = htmlspecialchars( $_SESSION["idDoc"]);

require("../bdd/bddconfig.php");

try {
    $objBdd = new PDO("mysql:host=$bddserver;dbname=$bddname;charset=utf8", $bddlogin, $bddpass);
    $objBdd->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    $PDOinsert = $objBdd->prepare("UPDATE `document` SET `nom` = :nom , `url` = :url , `type` = :type WHERE `document`.`idDoc` = $idDoc;");
    $PDOinsert->bindParam(':nom', $nom, PDO::PARAM_STR);
    $PDOinsert->bindParam(':url', $url, PDO::PARAM_STR);
    $PDOinsert->bindParam(':type', $type, PDO::PARAM_STR);


    $PDOinsert->execute();

    // $valeur = $recupDoc->fetch();
    
} catch (Exception $prmE) {
    die("Erreur : " . $prmE->getMessage());
}




// $serveur = $_SERVER['HTTP_HOST'];
// $chemin = rtrim(dirname(htmlspecialchars($_SERVER['PHP_SELF'])), '/\\');
// $page = 'index.php';
// header("Location: http://$serveur$chemin/$page");


header("Location: ../index.php");

// UPDATE `document` SET `nom` = 'testq', `url` = 'testq', `type` = 'img' WHERE `document`.`idDoc` = 3;