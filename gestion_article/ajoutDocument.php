<?php

session_start();

$nom = htmlspecialchars($_POST["nomDoc"]);
$url = htmlspecialchars($_POST["url"]);
$type = htmlspecialchars($_POST["type"]);

echo $_SESSION['idArticleAddDoc'];

$idArticle = $_SESSION['idArticleAddDoc'];

require('../bdd/bddconfig.php');

try {

    $objBdd = new PDO("mysql:host=$bddserver;dbname=$bddname;charset=utf8", $bddlogin, $bddpass);
    $objBdd->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    $PDOinsert = $objBdd->prepare("INSERT INTO `document` ( nom, url, type, article_idArticle) VALUES (:nom, :url, :type, :idArticle)");
        $PDOinsert->bindParam(':nom', $nom, PDO::PARAM_STR);
        $PDOinsert->bindParam(':url', $url, PDO::PARAM_STR);
        $PDOinsert->bindParam(':type', $type, PDO::PARAM_STR);
        $PDOinsert->bindParam(':idArticle', $idArticle, PDO::PARAM_STR);


    $PDOinsert->execute();


} catch (Exception $prmE) {
    die('Erreur : ' . $prmE->getMessage());
}

// $serveur = $_SERVER['HTTP_HOST'];
// $chemin = rtrim(dirname(htmlspecialchars($_SERVER['PHP_SELF'])), '/\\');
// $page = 'index.php';
// header("Location: http://$serveur$chemin/$page");


header("Location: ../index.php");