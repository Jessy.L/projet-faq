<?php 

$idUser = htmlspecialchars($_POST["user"]);


require("../bdd/bddconfig.php");

try {
    $objBdd = new PDO("mysql:host=$bddserver;dbname=$bddname;charset=utf8", $bddlogin, $bddpass);
    $objBdd->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    $recup = $objBdd->query("DELETE FROM `user` WHERE `user`.`idUser` = $idUser");
} catch (Exception $prmE) {
    die("Erreur : " . $prmE->getMessage());
}


header("Location: ../index.php");


