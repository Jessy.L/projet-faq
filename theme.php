<?php

$numTheme = $_GET['idTheme'];


?>


<main>

    <?php

    require("bdd/bddconfig.php");

    try {
        $objBdd = new PDO("mysql:host=$bddserver;dbname=$bddname;charset=utf8", $bddlogin, $bddpass);
        $objBdd->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

        $recup = $objBdd->query("SELECT * FROM `article`,theme WHERE theme.idTheme = theme_idTheme AND theme.idTheme =" . $numTheme);


    } catch (Exception $prmE) {
        die("Erreur : " . $prmE->getMessage());
    }

    ?>

    <?php
    while ($article = $recup->fetch()) {
    
        if($article['acces'] == "public" || $article['user_idUser'] == $_SESSION["connexion"]){
    ?>
    
        <div class="box">

            <div class="entete">
                <p><?php echo $article['titre']; ?></p>
                <p><?php echo $article['datePub']; ?></p>
            </div>

            <div class="texte">
                <p><?php echo $article['texte']; ?></p>
            </div>

            <a href="index.php?page=article&idArticle=<?php echo $article['idArticle'] ?>">Voir l'article</a>
        </div>

        <?php 
        }
        ?>


       

    <?php
    }
    $recup->closeCursor();
    ?>


    <?php
    if (isset($_SESSION['logged_in']['fonction'])) {

        if ($_SESSION['logged_in']['fonction'] == "tech") {
    ?>
            <a href="index.php?page=ajout_article&value=<?php echo $_GET['value'] ?>&idTheme=<?php echo $_GET['idTheme']; ?>">Ajouter un article</a>
    <?php
        }
    }
    ?>

</main>