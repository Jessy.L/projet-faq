<main>


    <?php

    require("bdd/bddconfig.php");

    try {
        $objBdd = new PDO("mysql:host=$bddserver;dbname=$bddname;charset=utf8", $bddlogin, $bddpass);
        $objBdd->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

        $recup = $objBdd->query("SELECT * FROM `article` WHERE idArticle =" . $_GET["idArticle"]);

        $recupDoc = $objBdd->query("SELECT * FROM `document` WHERE article_idArticle =" . $_GET["idArticle"]);
    } catch (Exception $prmE) {
        die("Erreur : " . $prmE->getMessage());
    }


    ?>

    <?php while ($article = $recup->fetch()) { ?>


        <?php
        if (isset($_SESSION['logged_in']['login'])) {
            if ($article['user_idUser'] == $_SESSION['logged_in']['id']) {
        ?>
                <a href="index.php?page=gestionArticleUser&idArticle=<?php echo $article['idArticle'] ?>">Modifier/Supprimer</a>
        <?php
            }
        }
        ?>

        <h2><?php echo $article['titre']; ?></h2>
        <p><?php echo $article['datePub']; ?></p>


        <div>
            <p><?php echo nl2br($article['texte']); ?></p>
        </div>

        <div>
            <?php
            while ($lienDoc = $recupDoc->fetch()) {
            ?>

                <?php 
                if( $lienDoc["type"] == "lien"){
                ?>

                    <li class="lienDoc">
                        <a href="<?php echo $lienDoc["url"]; ?>" target="_blank"> <?php echo $lienDoc["nom"] ?></a>
                    </li>
                
                <?php 
                }else if($lienDoc["type"] == "img"){
                ?>

                    <img src="<?php echo $lienDoc["url"]; ?>" alt="<?php echo $lienDoc["nom"] ?>">
                
                <?php 
                }else if($lienDoc["type"] == "fichier"){
                ?>

                    <li class="lienDoc">
                        <a href="doc/<?php echo $lienDoc["url"]; ?>" target="_blank"> <?php echo $lienDoc["nom"] ?></a>
                    </li>

                <?php 
                }else{
                    echo "ERROR TYPE INCORRECT";
                }
                ?>

            <?php
            }
            $recupDoc->closeCursor()
            ?>
        </div>

    <?php
    }
    $recup->closeCursor();
    ?>




</main>