<?php 

try {
    $objBdd = new PDO("mysql:host=$bddserver;dbname=$bddname;charset=utf8", $bddlogin, $bddpass);
    $objBdd->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    $recup = $objBdd->query("SELECT * FROM `theme`");

} catch (Exception $prmE) {
    die("Erreur : " . $prmE->getMessage());
}

?>


<main>

    <form method="POST" action="gestion_theme/modifTheme.php">


        <select name="theme">
            
            <?php
            while ($lien = $recup->fetch()) {
            ?>

                <option value="<?php echo $lien["idTheme"] ?>"><?php echo $lien["nom"]?></option>

            <?php
            };
            $recup->closeCursor();
            ?>
        </select>

        <label for="nomTheme">Nom du thème</label>    
        <input type="text" name="nomTheme">

        <input type="submit" value="MODIFIER">

    </form>

</main>