<?php

require("bdd/bddconfig.php");

try {
    $objBdd = new PDO("mysql:host=$bddserver;dbname=$bddname;charset=utf8", $bddlogin, $bddpass);
    $objBdd->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    $recup = $objBdd->query("SELECT * FROM `article` ORDER BY datePub DESC LIMIT 5");
} catch (Exception $prmE) {
    die("Erreur : " . $prmE->getMessage());
}

?>

<main>

    <div class="allmessage">

        <?php
        while ($messageSimple = $recup->fetch()) {
        ?>

            <div class="box">

                <div class="entete">
                    <p><?php echo $messageSimple['titre']; ?></p>
                    <p><?php echo $messageSimple['datePub']; ?></p>
                </div>

                <div class="texte">
                    <p><?php echo $messageSimple['texte']; ?></p>
                </div>

                <a href="index.php?page=article&idArticle=<?php echo $messageSimple['idArticle'] ?>">Voir l'article</a>
            </div>
            
        <?php
        }
        $recup->closeCursor();
        ?>

    </div>

</main>