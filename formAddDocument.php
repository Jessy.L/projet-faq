<?php


try {
    $objBdd = new PDO("mysql:host=$bddserver;dbname=$bddname;charset=utf8", $bddlogin, $bddpass);
    $objBdd->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    $recupDoc = $objBdd->query("SELECT * FROM `document` WHERE article_idArticle =" . $_SESSION['idArticleAddDoc']);
} catch (Exception $prmE) {
    die("Erreur : " . $prmE->getMessage());
}


?>

<main>

    <form method="POST" action="gestion_article/ajoutDocument.php">

        <label for="nom">Nom</label>
        <input type="text" name="nomDoc">

        <label for="url">URL</label>
        <input type="text" name="url">

        <label for="type">Type de fichier : </label>
        <select name="type" id="type">
            <option value="img">Image</option>
            <option value="lien">Lien</option>
            <option value="fichier">Fichier</option>
        </select>

        <input type="submit" value="Envoyer">

    </form>

    <div>
        <?php
        while ($lienDoc = $recupDoc->fetch()) {

        ?>
            <div>

                <?php echo $lienDoc["idDoc"]; ?>

                <a href="<?php echo $lienDoc["url"]; ?>" target="_blank"> <?php echo $lienDoc["nom"] ?> </a>
                
                <a href="index.php?page=formEditDocument&idDoc=<?php echo $lienDoc["idDoc"] ?>">Modifier le document</a>

                <form method="POST" action="gestion_Article/deleteDocument.php">
                    <input type="hidden" name="idDoc" value="<?php echo $lienDoc["idDoc"] ?>">
                    <input type="Submit" value="Supprimer le document">
                </form>
            </div>

        <?php
        }
        $recupDoc->closeCursor()
        ?>
    </div>


</main>