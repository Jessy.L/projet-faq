
<?php 


$idDoc = $_GET["idDoc"];
$_SESSION["idDoc"] = $idDoc;
$idArticle = $_SESSION['idArticleAddDoc']; 


try {
    $objBdd = new PDO("mysql:host=$bddserver;dbname=$bddname;charset=utf8", $bddlogin, $bddpass);
    $objBdd->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    $recupDoc = $objBdd->query("SELECT * FROM `document` WHERE article_idArticle = $idArticle AND idDoc = $idDoc" );

    $valeur = $recupDoc->fetch();
    
} catch (Exception $prmE) {
    die("Erreur : " . $prmE->getMessage());
}

?>



<main>

<form method="POST" action="gestion_article/editDocument.php">

    <label for="nom">Nom</label>
    <input type="text" name="nom" value="<?php echo $valeur["nom"] ?>">

    <label for="url">URL</label>
    <input type="text" name="url" value="<?php echo $valeur["url"] ?>">

  
    <label for="type">Type de fichier : </label>
    <select name="type" id="type">
        <option value="img">Image</option>
        <option value="lien">Lien</option>
        <option value="fichier">Fichier</option>
    </select>

    <input type="submit" value="VALIDER">

</form>

</main>