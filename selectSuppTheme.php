<?php

require("bdd/bddconfig.php");

try {
    $objBdd = new PDO("mysql:host=$bddserver;dbname=$bddname;charset=utf8", $bddlogin, $bddpass);
    $objBdd->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    $recup = $objBdd->query("SELECT * FROM `theme` ");
} catch (Exception $prmE) {
    die("Erreur : " . $prmE->getMessage());
}

?>

<main>

    <h2>SUPPRIMER UN THEME</h2>

    <form method="POST" action="gestion_theme/suppTheme.php">

        <select name="theme">
        
            <?php
            while ($lien = $recup->fetch()) {
            ?>

                <option value="<?php echo $lien["idTheme"] ?>"><?php echo $lien["nom"]?></option>

            <?php
            };
            $recup->closeCursor();
            ?>
        </select>

        <input type="submit" value="SUPPRIMER">

    </form>    
</main>
